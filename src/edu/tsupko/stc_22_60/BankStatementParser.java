package edu.tsupko.stc_22_60;

import java.util.List;

public interface BankStatementParser {
	BankTransaction parseFrom(String line);

	List<BankTransaction> parseLinesFrom(List<String> lines);
}
