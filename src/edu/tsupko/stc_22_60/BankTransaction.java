package edu.tsupko.stc_22_60;

import java.time.LocalDate;

public record BankTransaction(LocalDate date, double amount, String description) {
}
