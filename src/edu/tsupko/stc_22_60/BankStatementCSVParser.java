package edu.tsupko.stc_22_60;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class BankStatementCSVParser implements BankStatementParser {
	private static final String DELIMITER = ",";
	private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("dd.MM.yy");

	public BankTransaction parseFrom(String line) {
		var columns = line.split(DELIMITER);
		var date = LocalDate.parse(columns[0], DATE_PATTERN);
		var amount = Double.parseDouble(columns[1]);
		var description = columns[2];
		return new BankTransaction(date, amount, description);
	}

	public List<BankTransaction> parseLinesFrom(List<String> lines) {
		return lines.stream().map(this::parseFrom).toList();
	}
}
