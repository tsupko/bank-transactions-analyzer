package edu.tsupko.stc_22_60;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Month;

public class BankTransactionsAnalyzer {
	private static final String RESOURCES = "resources" + System.getProperty("file.separator");

	private static void collectSummary(BankStatementProcessor processor) {
		System.out.printf("Общая сумма транзакций равна %.2f \u20bd%n", processor.calculateTotalAmount());
		System.out.printf("Общая сумма транзакций в январе равна %.2f \u20bd%n", processor.calculateTotalInMonth(Month.JANUARY));
		System.out.printf("Общая сумма транзакций в феврале равна %.2f \u20bd%n", processor.calculateTotalInMonth(Month.FEBRUARY));
		System.out.printf("Общая сумма транзакций в марте равна %.2f \u20bd%n", processor.calculateTotalInMonth(Month.MARCH));
		System.out.printf("Общая сумма транзакций в категории \"Аванс\" равна %.2f \u20bd%n", processor.calculateTotalForCategory("Аванс"));
	}

	public void analyze(String filename, BankStatementParser parser) throws IOException {
		var path = Path.of(RESOURCES + filename);
		var lines = Files.readAllLines(path);

		var transactions = parser.parseLinesFrom(lines);

		var processor = new BankStatementProcessor(transactions);

		collectSummary(processor);
	}
}
