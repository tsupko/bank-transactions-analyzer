package edu.tsupko.stc_22_60;

import java.io.IOException;

public class MainApplication {
	public static void main(String[] args) throws IOException {
		var analyzer = new BankTransactionsAnalyzer();
		var parser = new BankStatementCSVParser();
		analyzer.analyze(args[0], parser);
	}
}
